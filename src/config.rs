use crate::macros::enum_error;
use crate::pages::PageDefinition;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fs::OpenOptions;
use std::path::Path;

enum_error! {
    ConfigError {
        SerdeError(serde_yaml::Error),
        IoError(std::io::Error)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Config<P> {
    pub endpoint: String,
    pub pages: Vec<PageDefinition<P>>,
}

impl<T: DeserializeOwned> Config<T> {
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, ConfigError> {
        let file = OpenOptions::new()
            .read(true)
            .create(false)
            .write(false)
            .open(path)?;
        let config = serde_yaml::from_reader(&file)?;

        Ok(config)
    }
}

impl<T: Serialize> Config<T> {
    pub fn write_path<P: AsRef<Path>>(path: P) -> Result<(), ConfigError> {
        let file = OpenOptions::new()
            .read(false)
            .write(true)
            .create(true)
            .open(path)?;
        let conf = Self::default();

        serde_yaml::to_writer(file, &conf)?;

        Ok(())
    }
}

impl<T> Default for Config<T> {
    fn default() -> Self {
        Config {
            endpoint: "changeme".to_string(),
            pages: vec![],
        }
    }
}
