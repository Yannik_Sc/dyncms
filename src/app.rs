use crate::config::{Config, ConfigError};
use crate::macros::enum_error;
use crate::pages::IntoPageContent;
use actix_web::{web, App as ActixApp, HttpRequest, HttpServer};
use serde::de::DeserializeOwned;
use serde::Serialize;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::sync::Arc;

///
/// Bootstraps the app with the given PageType and runs it.
///
/// # Example
///
/// ```rust,no_run
///
/// dyncms::content_types! {
///     PageTypes {}
/// }
///
/// #[dyncms::main]
/// async fn main() {
///     dyncms::run::<PageTypes, _>(std::env::args());
/// }
///
/// ```
///
/// # Arguments
///
/// You can run the app by simply using
///
/// ```bash,no_run
/// my_app ./config.yaml
/// ```
///
/// where ./config.yaml can be any compatible config yaml file. If you don't have one yet, you can
/// run
///
/// ```bash,no_run
/// my_app genconf ./config.yaml
/// ```
///
/// This will create a new, default config. The file path in this case can be omitted, it then
/// defaults to `./config.yaml`
///
/// If you just want to check your config file without starting the whole webserver (e.g. for CI
/// purpose) you can run
///
/// ```bash,no_run
/// my_app ckconf ./config.yaml
/// ```
///
/// In this case however the config path has to be supplied.
///
pub async fn run<
    T: 'static + Serialize + DeserializeOwned + IntoPageContent + Clone + Send + Sync,
    A: Iterator<Item = String>,
>(
    args: A,
) {
    match App::<T>::new(args) {
        Ok(app) => {
            if let Err(error) = app.run().await {
                match error {
                    AppError::Noop(_) => return,
                    AppError::Message(str) => {
                        eprintln!("{}", str);
                        std::process::exit(1);
                    }
                    e => {
                        eprintln!("Could not run app: {}", e);
                        std::process::exit(1);
                    }
                }
            }
        }
        Err(error) => match error {
            AppError::Noop(_) => return,
            AppError::Message(str) => {
                eprintln!("{}", str);
                std::process::exit(1);
            }
            e => {
                eprintln!("Could not create app: {}", e);
                std::process::exit(1);
            }
        },
    }
}

pub struct App<T>(Vec<String>, Config<T>);

#[derive(Debug)]
pub struct NoopError;

impl Display for NoopError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("No-Op")
    }
}

impl Error for NoopError {}

enum_error! {
    AppError {
        ConfigError(ConfigError),
        IoError(std::io::Error),
        Message(String),
        Noop(NoopError)
    }
}

impl<T: 'static + Serialize + DeserializeOwned + IntoPageContent + Clone + Send + Sync> App<T> {
    pub fn new<A: Iterator<Item = String>>(args: A) -> Result<Self, AppError> {
        let args: Vec<String> = args.collect();

        match args.get(1).and_then(|val| Some(val.as_str())) {
            Some("genconf") => {
                let config_path = args
                    .get(2)
                    .map(String::clone)
                    .unwrap_or_else(|| String::from("config.yaml"));

                Config::<T>::write_path(&config_path)?;

                println!("Generated minimal config: {}", &config_path);

                return Err(NoopError.into());
            }
            Some("ckconf") => {
                let config_path = args.get(2).map(String::clone);

                if let Some(config_path) = config_path {
                    Config::<T>::from_path(&config_path)?;

                    return Err(NoopError.into());
                }

                return Err(AppError::Message(String::from("Config file path missing")));
            }
            _ => {}
        }

        let config = Config::<T>::from_path(
            args.get(1)
                .map(String::clone)
                .unwrap_or_else(|| String::from("config.yaml")),
        )?;

        Ok(Self(args, config))
    }

    pub async fn run(&self) -> Result<(), AppError> {
        let pages = Arc::new(self.1.pages.clone());

        HttpServer::new(move || {
            let mut app = ActixApp::new();

            for definition in pages.as_ref() {
                let definition = definition.clone();
                app = app.service(web::resource(&definition.base_url).to(
                    move |req: HttpRequest| {
                        let mut content = definition.get_content();

                        async move { content.respond(req) }
                    },
                ));
            }

            app
        })
        .bind(&self.1.endpoint)?
        .run()
        .await?;

        Ok(())
    }
}
