//!
//! Easily build static or semi-dynamic pages
//!
//! # Example
//!
//! In the application you can define your PageTypes in a pattern similar to an enum (as this,
//! internally, is an enum). Each Content Type has to implement the Clone, Debug, serde::Deserialize
//! and serde::Serialize traits (the serde traits are re-exported for convenience).
//!
//! Note: Default types may come in future
//!
//! ```rust,no_run
//! use std::env::args;
//! use dyncms::{run, content_types, Serialize, Deserialize, PageContent, HttpResponse, HttpRequest};
//!
//! #[derive(Clone, Debug, Deserialize, Serialize)]
//! pub struct Text(String);
//!
//! impl PageContent for Text {
//!     fn respond(&mut self, _: HttpRequest) -> HttpResponse {
//!         HttpResponse::Ok().body(self.0.clone())
//!     }
//! }
//!
//! content_types! {
//!     PageTypes {
//!         Text(Text)
//!     }
//! }
//!
//! #[dyncms::main]
//! async fn main() {
//!     run::<PageTypes, _>(args()).await;
//! }
//! ```
//!
//! To let the application run you have to call it with a path to a config file (defaults to
//! `./config.yaml`), which (depending on the content types, but for this example) looks like the
//! following.
//!
//! ```yaml,no_run
//! endpoint: 0.0.0.0:8080
//! pages:
//!   - base_url: "/"
//!     content:
//!         Text: "Hello world"
//! ```
//!
//! If you want to generate a config file, you can call the application with a `genconf [file_path]`
//! where the `file_path` is an optional parameter, which defaults to `./config.yaml`.
//!
//! This example app shows a "Hello World" text when "http://0.0.0.0:8080/" is visited in the
//! browser.
//!
//! ## Predefined types
//!
//! For a predefined PageTypes enum you can check out the [pages::DefaultPageTypes] enum. If you
//! want more details on the specific variants you may want to look at [pages::Directory],
//! [pages::Markdown] and [pages::Redirect]
//!

pub mod app;
pub mod config;
pub mod pages;

pub use actix_web::{HttpRequest, HttpResponse};
#[doc(inline)]
pub use app::run;
#[doc(inline)]
pub use pages::{DefaultPageTypes, Directory, Markdown, PageContent, Redirect};
pub use serde::{Deserialize, Serialize};
pub use tokio::main;

pub mod macros {
    ///
    /// This macro allows to build a ContentType enum with all needed traits implemented.
    /// It also gives the option to extend an already existing ContentType enum.
    ///
    /// # Example
    ///
    /// Define page types from scratch
    ///
    /// ```rust
    /// use dyncms::{HttpRequest, HttpResponse, content_types, PageContent, Serialize, Deserialize};
    ///
    /// #[derive(Clone, Debug, Deserialize, Serialize)]
    /// struct Text(String);
    /// impl PageContent for Text {fn respond(&mut self, req: HttpRequest) -> HttpResponse {
    ///         todo!()
    ///     }
    /// }
    ///
    /// content_types! {
    ///     TestContents {
    ///         Text(Text)
    ///     }
    /// }
    /// ```
    ///
    /// Extend the DefaultPageTypes (or ony other, of cause)
    ///
    /// ```rust
    /// use dyncms::{
    ///     HttpRequest,
    ///     HttpResponse,
    ///     content_types,
    ///     PageContent,
    ///     Serialize,
    ///     Deserialize,
    ///     DefaultPageTypes
    /// };
    ///
    /// #[derive(Clone, Debug, Deserialize, Serialize)]
    /// struct Text(String);
    /// impl PageContent for Text {fn respond(&mut self, req: HttpRequest) -> HttpResponse {
    ///         todo!()
    ///     }
    /// }
    ///
    /// content_types! {
    ///     TestContents extends DefaultPageTypes {
    ///         Text(Text)
    ///     }
    /// }
    /// ```
    ///
    #[macro_export]
    macro_rules! content_types {
        ($type_name: ident { $($name: ident($type: ty)),*}) => {
            #[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
            pub enum $type_name {
                $(
                    $name($type),
                )*
            }

            impl $crate::pages::IntoPageContent for $type_name {
                fn into_page_content(self) -> Box<dyn $crate::pages::PageContent> {
                    match self {
                        $(
                            Self::$name(typ) => Box::new(typ),
                        )*
                    }
                }
            }

            unsafe impl Sync for $type_name {}
            unsafe impl Send for $type_name {}
        };

        ($type_name: ident extends $default_type: ident { $($name: ident($type: ty)),*}) => {
            paste::paste!{
                content_types!{
                    [<$type_name $default_type>] {
                        $(
                            $name($type)
                        ),*
                    }
                }

                #[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
                #[serde(untagged)]
                pub enum $type_name {
                    $type_name([<$type_name $default_type>]),
                    $default_type($default_type),
                }

                impl $crate::pages::IntoPageContent for $type_name {
                    fn into_page_content(self) -> Box<dyn $crate::pages::PageContent> {
                        match self {
                            Self::$type_name(typ) => typ.into_page_content(),
                            Self::$default_type(typ) => typ.into_page_content(),
                        }
                    }
                }

                unsafe impl Sync for $type_name {}
                unsafe impl Send for $type_name {}
            }
        };
    }

    #[macro_export]
    macro_rules! enum_error {
        ($type_name: ident { $($name: ident($type: ty)),* }) => {
            #[derive(Debug)]
            pub enum $type_name {
                $(
                    $name($type),
                )*
            }

            impl std::fmt::Display for $type_name {
                fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    match self {
                        $(
                            $type_name::$name(error) => std::fmt::Display::fmt(
                                error,
                                formatter
                            ),
                        )*
                    }
                }
            }

            impl std::error::Error for $type_name {}

            $(
                impl std::convert::From<$type> for $type_name {
                    fn from(error: $type) -> Self {
                        Self::$name(error)
                    }
                }
            )*
        };
    }

    pub use {content_types, enum_error};
}
