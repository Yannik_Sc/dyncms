use crate::pages::{IntoPageContent, PathOrContent};
use crate::{Directory, Markdown, PageContent, Redirect};
use actix_web::{HttpRequest, HttpResponse};
use comrak::{ComrakExtensionOptions, ComrakOptions, ComrakParseOptions, ComrakRenderOptions};
use std::ops::Index;

impl PageContent for Directory {
    fn respond(&mut self, req: HttpRequest) -> HttpResponse {
        let matches = req.match_info();

        if matches.segment_count() < 1 {
            return HttpResponse::NotFound().body("Not Found: D1");
        }

        let resource = matches.index(0);

        if resource.starts_with("/") {
            return HttpResponse::Forbidden().body("Forbidden: D1");
        }

        let resource = self.0.join(resource);

        if !resource.exists() || resource.is_dir() {
            return HttpResponse::NotFound().body("Not Found: D2");
        }

        let bytes = match std::fs::read(&resource) {
            Ok(content) => content,
            Err(error) => {
                eprintln!("Could not read file: {}", error);

                return HttpResponse::InternalServerError().body("Internal Server Error: D2");
            }
        };

        let mime_type = match mime_guess::from_path(&resource).first() {
            None => String::from("text/plain"),
            Some(mime) => String::from(mime.essence_str()),
        };

        HttpResponse::Ok()
            .append_header(("Content-Type", mime_type))
            .body(bytes)
    }
}

impl PageContent for Markdown {
    fn respond(&mut self, _: HttpRequest) -> HttpResponse {
        let content = match self.0.clone().into_content() {
            Ok(content) => content,
            Err(error) if error.kind() == std::io::ErrorKind::NotFound => {
                eprintln!("Could not open Markdown file: {}", error);

                return HttpResponse::NotFound().body("Not Found: M1");
            }
            Err(err) => {
                eprintln!("Could not read Markdown file: {}", err);

                return HttpResponse::InternalServerError().body("Internal Server Error: M1");
            }
        };

        let html = comrak::markdown_to_html(
            content.as_str(),
            &ComrakOptions {
                extension: ComrakExtensionOptions {
                    strikethrough: true,
                    tagfilter: true,
                    table: true,
                    autolink: true,
                    tasklist: true,
                    superscript: true,
                    header_ids: Some(String::from("dyncms-")),
                    footnotes: true,
                    description_lists: true,
                    front_matter_delimiter: None,
                },
                parse: ComrakParseOptions {
                    smart: true,
                    default_info_string: None,
                },
                render: ComrakRenderOptions {
                    hardbreaks: false,
                    github_pre_lang: true,
                    width: 0,
                    unsafe_: true,
                    escape: false,
                },
            },
        );

        HttpResponse::Ok()
            .append_header(("Content-Type", "text/html"))
            .body(html)
    }
}

impl PageContent for Redirect {
    fn respond(&mut self, _: HttpRequest) -> HttpResponse {
        HttpResponse::PermanentRedirect()
            .append_header(("Location", self.0.as_str()))
            .body("")
    }
}

impl PathOrContent {
    pub fn into_content(self) -> std::io::Result<String> {
        match self {
            Self::Content { content } => Ok(content),
            Self::Path(path) => std::fs::read_to_string(path),
        }
    }
}

impl<T: PageContent> PageContent for Box<T> {
    fn respond(&mut self, req: HttpRequest) -> HttpResponse {
        (**self).respond(req)
    }
}

impl<T: IntoPageContent> IntoPageContent for Box<T> {
    fn into_page_content(self) -> Box<dyn PageContent> {
        (*self).into_page_content()
    }
}
