use crate::content_types;
use actix_web::{HttpRequest, HttpResponse};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

pub mod default_page_types;

///
/// Serves the content of a whole directory.
/// MimeType parsing is done with [mime_guess]
///
/// # Note
///
/// The resource path has to and with **one** catch-all pattern (e.g. `{path:.*}`). The name does not
/// matter though.
///
/// # Example
///
/// ```yaml,no_run
/// pages:
///   - base_url: "/resources/{path:.*}"
///     content:
///       Directory: "resources"
/// ```
///
/// # Tests
///
/// ```rust
/// use std::path::PathBuf;
/// use actix_web::body::MessageBody;
/// use dyncms::{Directory, PageContent};
///
/// let request = actix_web::test::TestRequest::with_uri("/").param("dummy", "test.file").to_http_request();
/// let mut dir = Directory(PathBuf::from("resources"));
/// let res = dir.respond(request);
///
///
/// assert_eq!(res.status(), 200);
/// let bytes = res.into_body().try_into_bytes().unwrap();
///
/// let response = String::from_utf8_lossy(bytes.as_ref()).to_string();
/// assert!(response.eq("success"));
/// ```
///
/// ```rust
/// use std::path::PathBuf;
/// use dyncms::{Directory, PageContent};
///
/// let request = actix_web::test::TestRequest::with_uri("/").param("dummy", "not_found_file").to_http_request();
/// let mut dir = Directory(PathBuf::from("resources"));
/// let res = dir.respond(request);
///
/// assert_eq!(res.status(), 404);
/// ```
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Directory(pub PathBuf);

///
/// Serves a rendered Markdown file.
/// Rendering is done with comrak.
///
/// # Example
///
/// ```yaml,no_run
/// pages:
///   - base_url: "/markdown-test"
///     content:
///       Markdown: "resources/test.md"
/// ```
///
/// # Tests
///
/// ```rust
/// use std::path::PathBuf;
/// use actix_web::body::{MessageBody};
/// use dyncms::{Markdown, PageContent};
/// use dyncms::pages::PathOrContent;
///
/// let request = actix_web::test::TestRequest::with_uri("/").to_http_request();
/// let mut md = Markdown(PathOrContent::Path(PathBuf::from("resources/test.md")));
/// let res = md.respond(request);
///
/// assert_eq!(res.status(), 200);
///
/// let bytes = res.into_body().try_into_bytes().expect("Test body could not converted to bytes");
///
/// let response = String::from_utf8_lossy(bytes.as_ref()).to_string();
///
/// assert!(response.contains("<h1><a href=\"#test\" aria-hidden=\"true\" class=\"anchor\" id=\"dyncms-test\"></a>Test</h1>"));
/// ```
///
/// ```rust
/// use std::path::PathBuf;
/// use dyncms::{Markdown, PageContent};
/// use dyncms::pages::PathOrContent;
///
/// let request = actix_web::test::TestRequest::with_uri("/").to_http_request();
/// let mut md = Markdown(PathOrContent::Path(PathBuf::from("resources/not_found.md")));
/// let res = md.respond(request);
///
/// assert_eq!(res.status(), 404);
/// ```
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Markdown(pub PathOrContent);

///
/// Responds with a permanent redirect (308) to the configured page/url.
///
/// # Example
///
/// ```yaml,no_run
/// pages:
///   - base_url: "/example"
///     content:
///       Redirect: "https://example.org"
/// ```
///
/// # Tests
///
/// ```rust
/// use std::path::PathBuf;
/// use dyncms::{Redirect, PageContent};
///
/// let request = actix_web::test::TestRequest::default().to_http_request();
/// let mut rdr = Redirect(String::from("https://yannik-sc.de"));
/// let res = rdr.respond(request);
///
/// assert_eq!(res.status(), 308);
/// assert_eq!(res.headers().get("Location").expect("Location was not set in test"), "https://yannik-sc.de");
/// ```
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Redirect(pub String);

///
/// Resolves a config value to be directly the content of a file, or a file path.
///
/// Provides function to resolve itself into content (by eiter returning its content variant, or
/// reading the file path).
///
/// # Example
///
/// ```yaml,no_run
/// pages:
///   - base_url: "/from_path" # Loads the file content
///     content:
///       Markdown: "resources/test.md"
///   - base_url: "/from_value" # Gets the content by its value
///     content:
///       Markdown:
///         content: "# Hello markdown!"
///
/// ```
///
#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum PathOrContent {
    Path(PathBuf),
    Content { content: String },
}

pub trait PageContent {
    fn respond(&mut self, req: HttpRequest) -> HttpResponse;
}

pub trait IntoPageContent {
    fn into_page_content(self) -> Box<dyn PageContent>;
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PageDefinition<P> {
    pub base_url: String,
    pub content: P,
}

impl<P: 'static + IntoPageContent + Clone> PageDefinition<P> {
    pub(crate) fn get_content(&self) -> Box<dyn PageContent> {
        self.content.clone().into_page_content()
    }
}

content_types! {
    DefaultPageTypes {
        Markdown(Markdown),
        Redirect(Redirect),
        Directory(Directory)
    }
}
