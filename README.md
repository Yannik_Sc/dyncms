# DynCMS

A dynamic CMS (at least that is the plan).

## Planned Features

- Define URLs
- Assign actions to URLs
  - Supported actions should be:
    - Render a markdown file (with support of variables)
    - Render a predefined struct/template
    - Raw HTML files
    - Serve files from a directory
