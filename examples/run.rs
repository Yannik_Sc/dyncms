//! This example is meant to shows off all (or at least most of the) available features
//! that this library provides.

use dyncms::{
    content_types, run, DefaultPageTypes, Deserialize, HttpRequest, HttpResponse, PageContent,
    Serialize,
};
use std::env::args;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Text(String);

impl PageContent for Text {
    fn respond(&mut self, _: HttpRequest) -> HttpResponse {
        HttpResponse::Ok().body(self.0.clone())
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Text2(String);

impl PageContent for Text2 {
    fn respond(&mut self, _: HttpRequest) -> HttpResponse {
        HttpResponse::Ok().body(self.0.clone())
    }
}

content_types! {
    PageTypes extends DefaultPageTypes {
        Text(Text)
    }
}

content_types! {
    PageTypes2 extends PageTypes {
        Text2(Text2)
    }
}

#[dyncms::main]
async fn main() {
    run::<PageTypes2, _>(args()).await;
}
